<p align="center">
  <img src="truepasta_logo.svg" alt="Toolkit für randomisierte Übungs- und Prüfungsaufgaben in der Statistiklehre" width="50%" />
</p>

# Seiteninhalt

- [Über das Projekt](#über-das-projekt)
- [Installation](#installation)
- [Initialisierung](#initialisierung)
- [Anpassung](#anpassung)
- [Veröffentlichung](#veröffentlichung)

# Über das Projekt

Gute Übungsaufgaben mit nachvollziehbaren Musterlösungen sind essenziell für den Erwerb und die Überprüfung von Kompetenzen in der Anwendung statischer Verfahren. Das Projekt "truepasta" reagiert auf den Mangel an Aufgaben, indem es die Erstellung von Fragestellungen und Lösungswegen erleichtert.

"truepasta" wurde im Rahmen eines [DigiTeLL](https://www.uni-frankfurt.de/106206707/Projekt_DigiTeLL)-Partnerships von Till Straube und Malenka Nicol im Wintersemester 2023/24 entwickelt.

Für jegliche Rückmeldungen stehen wir jederzeit gerne zur Verfügung: <straube@geo.uni-frankfurt.de>

# Installation

Das R-Paket `truepasta` kann direkt aus dem vorliegenden Git-Repository installiert werden:

```r
# Paket "devtools" installieren, falls nötig
install.packages("devtools")

# Paket "truepasta" installieren
devtools::install_git("https://codeberg.org/ktiu/truepasta")
```

# Initialisierung

Mit dem folgenden Befehl wird die benötigte Ordnerstruktur im Projektordner erstellt:

```r
truepasta::initialize()
```

Die meisten erstellten Dateien dienen dabei der Darstellung des Web Interfaces (mittels [Jekyll](https://jekyllrb.com/)).

# Anpassung

## Metadaten

In der Datei `_config.yml` die eigenen Informationen hinterlegen. Dabei die Kommentare in der Datei beachten.

## Aufgabenparameter

Die wichtigste Anpassung betrifft die Inhalte des Ordners `_input`. Darin befinden sich die Definitionen der domänenspezifischen Szenarien, die der Generierung der Textaufgaben zugrunde liegen. Die Definitionen sind in zwei Ordner geteilt:

### Szenarien (`_input/scenarios/`)

Jede Datei ist ein Szenario, das Ausgangspunkt für verschiedene Aufgabentypen sein kann. Sofern für ein Szenario passende Variablen hinterlegt sind, können entsprechende Fragerichtungen in der Datei hinterlegt werden.

**Die vorhandenen Dateien dienen als Beispiele und sollten vor Generieren gelöscht werden!**

### Namen (`_input/names/`)

Hier können Listen (etwa von Ortsnamen) hinterlegt werden, auf die von verschiedenen Szenarien zugegriffen werden kann.

## Weiterführend: Aufgabentypen, Templates

Wenn statt oder neben [HTML-Fragmenten](https://rmarkdown.rstudio.com/html_fragment_format.html) für das Web Interface andere Formate ausgegeben werden sollen, dann ist das grundsätzlich möglich.

Auch neue Aufgabentypen können erstellt werden.

Hierfür müssen jedoch bisher Änderungen am Paket selber vorgenommen werden (genauer in der Funktion `generate()`).

# Generierung

Mit dem folgenden Befehl werden für alle hinterlegten Aufgabentypen Fragen und Musterlösungen vorgeneriert:

```r
truepasta::generate()
```

Dabei lässt sich die Anzahl der Aufgaben je Aufgabentyp anpassen:

```r
truepasta::generate(1000)
```

Der Standardwert ohne Angabe ist 100 Aufgaben pro Typ.

# Veröffentlichung

## Die kurze Antwort

Der komplette Projektordner lässt sich als [Github page](https://pages.github.com/) veröffentlichen.

## Die lange Antwort

Der Projektordner ist der Quellcode für das Web-Interface. Mit [Jekyll](https://jekyllrb.com/) lässt sich daraus eine statische Webseite generieren. Es empfiehlt sich, Jekyll lokal zu installieren und die korrekte Darstellung zu prüfen, bevor das Web Interface veröffentlicht wird.

Die resultierende Webseite (also nach Seitenaufbau mit Jekyll die Inhalte des Ordners `_site`) enthält nur HTML, JavaScript und CSS und lässt sich so bequem auf jedem Webserver hinterlegen.

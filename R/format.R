multi <- function(x) {
  strwrap(x, width = Inf)
}

tabelle <- function(x, ..., full_width = T, header_above = NULL, sum_row = F,
                    sum_column = F, row_head = F, escape = F, hold = F, longtable = F,
                    align_header_above = "c") {
  target <- knitr::opts_knit$get("rmarkdown.pandoc.to")
  if (!is.null(target)) {
    if (target == "html") {
      knitr::kable(x,
        ...,
        format = "html",
        escape = escape
      ) -> wip
    } else if (target == "latex") {
      knitr::kable(x,
        ...,
        format = "latex",
        booktab = T,
        linesep = "",
        longtable = longtable,
        escape = escape
      ) -> wip
    }
    if (full_width && (target == "latex")) {
      map(seq_along(x), \(i) {
        list(
          col = i,
          length = map_int(x[[i]], stringr::str_length) %>%
            max()
        )
      }) %>%
        rlist::list.filter(length > 20) -> long
      purrr::reduce(long, \(acc, c) {
        kableExtra::column_spec(acc, c$col, width = "8cm")
      }, .init = wip) -> wip
    }
    if (!is.null(header_above)) {
      wip %<>%
        kableExtra::add_header_above(header_above,
          bold = T,
          align = align_header_above
        )
    }
    if (sum_row) {
      row_count <- nrow(x)
      wip %<>%
        kableExtra::row_spec(row_count, bold = T) %>%
        kableExtra::row_spec(row_count - 1,
          hline_after = T,
          extra_css = "border-bottom: 1px solid; "
        )
    }
    if (sum_column) {
      col_count <- ncol(x)
      wip %<>%
        kableExtra::column_spec(col_count, bold = T) %>%
        kableExtra::column_spec(col_count - 1, border_right = T)
    }
    if (row_head) {
      wip %<>%
        kableExtra::column_spec(1, bold = T) %>%
        kableExtra::column_spec(1, border_right = T)
    }
    if (target == "html") {
      wip %>%
        kableExtra::row_spec(0, extra_css = "border-bottom: 1px solid; ") %>%
        kableExtra::kable_styling(full_width = full_width) %>%
        kableExtra::scroll_box(box_css = "overflow-y: auto;")
    } else if (target == "latex") {
      opts <- c("striped")
      if (hold) opts <- c(opts, "HOLD_position")
      if (longtable) opts <- c(opts, "repeat_header")
      wip %>%
        kableExtra::row_spec(0, bold = T) %>%
        kableExtra::kable_styling(latex_options = opts)
    }
  } else {
    print("No target format")
  }
}


fmt <- function(number, digits = NULL) {
  rounded <- round(number, ifelse(is.null(digits), 2, digits))
  format(rounded,
    decimal.mark = "{,}",
    scientific = F
  )
}

percent <- function() {
  target <- knitr::opts_knit$get("rmarkdown.pandoc.to")
  if ((!is.null(target)) && target == "latex") {
    return("\\%")
  } else {
    return("%")
  }
}

symbol_header <- function(symbol) {
  target <- knitr::opts_knit$get("rmarkdown.pandoc.to")
  if ((!is.null(target)) && target == "latex") {
    list(
      "z" = "z",
      "t" = "t",
      "F" = "F",
      "sxy" = "s_{xy}",
      "r" = "r",
      "R2" = "R^2",
      "chi2" = "\\chi^2",
      "phi" = "\\phi"
    )[[symbol]] %>%
      sprintf("$%s$", .)
  } else {
    list(
      "z" = "𝑧",
      "t" = "𝑡",
      "F" = "𝐹",
      "sxy" = "𝑠<sub>𝑥𝑦</sub>",
      "r" = "𝑟",
      "R2" = "𝑅²",
      "chi2" = "𝜒²",
      "phi" = "𝜙"
    )[[symbol]] %>%
      sprintf("%s", .)
  }
}

fix_formula <- function(string) {
  string %<>%
    stringr::str_replace_all("(\\d),(\\d)", "\\1{,}\\2") %>%
    stringr::str_replace_all("- *-", "+") %>%
    stringr::str_replace_all("\\+ *-", "-") %>%
    stringr::str_replace_all("\\cdot *(-[0-9,{}]*)", "\\cdot \\(\\1\\)")
  target <- knitr::opts_knit$get("rmarkdown.pandoc.to")
  if ((!is.null(target)) && target == "latex") {
    string %<>%
      stringr::str_replace_all("\\\\lt", "<") %>%
      stringr::str_replace_all("\\\\gt", ">")
  }
  return(string)
}

solution_table <- function(solution_data) {
  summe <- sum(solution_data$Punkte)
  solution_data %>%
    dplyr::mutate(
      `Erreicht` = sprintf(
        paste(
          "<input type='checkbox'",
          "style='width:30px;",
          "height:30px' value='%s' />"
        ),
        stringr::str_replace(Punkte, ",", ".")
      ),
      `Max. Punkte` = format(Punkte, nsmall = 1),
      `&nbsp;` = ifelse(Implizit, "(auch&nbsp;implizit)", "")
    ) %>%
    dplyr::select(Schritt, `Musterlösung`, `&nbsp;`, `Max. Punkte`, `Erreicht`) %>%
    rbind(list(
      Schritt = "",
      `Musterlösung` = "",
      `&nbsp;` = "$\\sum$",
      `Max. Punkte` = format(summe, nsmall = 1),
      `Erreicht` = paste0(
        "<div class='punkte-aufgabe'>",
        "<input type='hidden' value='1'/>",
        "<span></span>"
      )
    )) %>%
    tabelle(escape = F, align = "lccc")
}

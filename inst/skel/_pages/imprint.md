---
permalink: /impressum
---

# Datenschutzerklärung

## Cookies

Diese Seite benutzt keine Cookies. Manche Einstellungen werden Browser-seitig im so genannten Local Storage gespeichert und können vom Server nicht ausgelesen werden.

## Hosting

Beim Hosting von Webseiten werden in so genannten Access Logs routinemäßig Daten gespeichert, die beim Zugriff auf die Seite entstehen. Hierin sind personenbezogene Daten wie IP-Adressen oder Informationen über den Browser enthalten.

{{ site.hosting_statement }}

Verantwortlich für den Betrieb des Webservers ist:

{{ site.hosting_address | newline_to_br }}

# Impressum

Verantwortlich für die Inhalte auf dieser Seite ist:

{{ site.name }}  
{{ site.affiliation | newline_to_br }}

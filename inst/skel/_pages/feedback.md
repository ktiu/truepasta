---
permalink: /feedback
---

# Feedback

Ich freue mich über jegliches Feedback zu dieser Seite und den Übungsaufgaben, damit ich sie verbessern kann.

Bitte schicken Sie zu diesem Zweck eine kurze E-Mail an [{{ site.email }}]({{ site.email }}) mit den folgenden Informationen:

- Was ist ihnen aufgefallen?
- Ist etwas fehlerhaft oder verwirrend formuliert? Was genau?
- Wie könnte eine Verbesserung aussehen?

Natürlich können Sie die E-Mail auch von einem anonymen Account schicken.

Vielen Dank!

--- *{{ site.name }}*

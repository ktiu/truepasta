var index

const populateSelection = (index) => {

  choices = document.getElementById('typeChoices')
  choices.textContent = ''

  index.forEach(type => {
    const entry = document.createElement('div')
    const checkBox = document.createElement('input')
    checkBox.setAttribute('type', 'checkbox')
    checkBox.setAttribute('name', 'choose_types')
    checkBox.setAttribute('name', 'choose_types')
    checkBox.checked = true
    checkBox.id = type.slug
    checkBox.addEventListener('change', updateSelectAll)
    const label = document.createElement('label')
    label.innerHTML = type.name
    label.setAttribute("for", type.slug)
    entry.appendChild(checkBox)
    entry.appendChild(label)
    choices.appendChild(entry)
  })
}

const checkAll = (e) => {
  boxes = [...document.querySelectorAll('input[name=choose_types]')]
  if (e.srcElement.indeterminate)  return

  if (e.srcElement.checked) {
    boxes.map(box => box.checked = true)
    return
  }

  boxes.map(box => box.checked = false)
}

const updateSelectAll = () => {

  boxCount = [...document.querySelectorAll('input[name=choose_types]')].length
  checkedCount = getChecked().length

  selectAll = document.getElementById('selectAll')

  if (checkedCount === 0) {
    selectAll.indeterminate = false
    selectAll.checked = false
  } else if (boxCount === checkedCount) {
    selectAll.indeterminate = false
    selectAll.checked = true
  } else {
    selectAll.checked = false
    selectAll.indeterminate = true
  }

}

const getChecked = () => {

  return [...document.querySelectorAll('input[name=choose_types]:checked')]
    .map(node => node.id)
}

const showNext = () => {

  const options = index
    .filter(type => getChecked().includes(type.slug))
    .flatMap(type => type.ids)

  if (options.length < 1) {
    alert('Keine Aufgaben gefunden. Bitte Auswahl erweitern (oder Fehler melden, indem Sie auf "Feedback" klicken).')
    return
  }

  const q = options[Math.floor(Math.random()*options.length)]

  const url = new URL(location);
  url.searchParams.set('q', q);
  history.pushState({}, "", url);

  showQuestion(q)

}

const showQuestion = (q) => {

  const [
    selection,
    question,
    questionText,
    answer,
    answerText
  ] = [
    'selection',
    'question',
    'questionText',
    'answer',
    'answerText'
  ].map(id => document.getElementById(id))

  Promise.all([
    fetch(`../../pool/html_questions/${q}.html`),
    fetch(`../../pool/html_answers/${q}.html`)
  ])

  .then(res => {
    if (res.every(r => r.ok)) {
      return Promise.all(res.map((r) => r.text()))
    } else {
      throw new Error()
    }
  })

  .catch(() => {
    return [
      'Fehler: Aufgabe nicht gefunden',
      ''
    ]
  })

  .then(([q, a]) => {
    hideLoading()
    hideAnswer()
    questionText.innerHTML = q
    answerText.innerHTML = a
    MathJax.typesetPromise()
    selection.classList.add('hidden')
    answer.classList.add('hidden')
    question.classList.remove('hidden')
  })
}

const hideLoading = () => {
    document.getElementById('loading').classList.add('hidden')
}

const showSelection = () => {
    document.getElementById('question').classList.add('hidden')
    document.getElementById('selection').classList.remove('hidden')
    hideLoading()
}

const hideAnswer = () => {
    document.getElementById('answer').classList.add('hidden')
    document.getElementById('reveal').textContent = 'Lösung anzeigen'
}

const showAnswer = () => {
    document.getElementById('answer').classList.remove('hidden')
    MathJax.typesetPromise()
    document.getElementById('reveal').textContent = 'Lösung verstecken'
}

const toggleAnswer = () => {
  const hidden = answer.classList.contains('hidden')
  if (hidden) {
    showAnswer()
  } else {
    hideAnswer()
  }
}

(async () => {

  const questions = await fetch('../../pool/questions.json')
  index = await questions.json()

  const urlParams = new URLSearchParams(window.location.search)

  populateSelection(index)

  if (urlParams.has('q')) {
    showQuestion(urlParams.get('q'))
  } else {
    showSelection()
  }

  document.getElementById('show').onclick = showNext
  document.getElementById('next').onclick = showNext
  document.getElementById('reveal').onclick = toggleAnswer
  document.getElementById('selectAll').addEventListener('change', checkAll)

})()

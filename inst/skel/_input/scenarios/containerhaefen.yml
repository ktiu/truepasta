meta:
  names_from: ports
  preposition: in
  ue: Schiff
  variables:
    Liegezeit:
      plural:
        - Liegezeiten
        - Abfertigungszeiten
        - Zeiten zwischen An- und Ablegen
      Welcherwert: Welche Liegezeit
      derwert: die Liegezeit
      einwert: eine Liegezeit
      Einwert: Eine Liegezeit
      allerwerte: aller Liegezeiten
      units_in:  Stunden
      precision: 1
      min:       5
      max:       50
      sigma:     [4, 6]
      mu:        [18, 20]
      rundewerte: [10, 20, 30, 40]
      pop_normal:
        - die Liegezeiten in allen Containerhäfen im Mittelmeer in etwa einer Normalverteilung folgen
        - die Zeit zwischen An- und Ablegen je Schiff über alle Mittelmeerhäfen annähernd normalverteilt ist
      scene:
        - In der Frachtschifffahrt bezeichnet die Liegezeit die Zeit, die zwischen Ankunft und Abfahrt eines Schiffes während Be- und Entladearbeiten verstreicht. Sie möchten die Liegezeiten von kleineren Containerschiffen in Mittelmeerhäfen untersuchen.
        - Sie sind beauftragt, die Abfertigungszeiten von kleineren Mittelmeerhäfen zu untersuchen. Die Zeit, die zwischen Ankunft und Abfahrt eines Schiffes während Be- und Entladearbeiten verstreicht, wird als Liegezeit bezeichnet.
    Umschlag:
      plural:
        - Umschlagzahlen pro Schiff
        - Umschlagzahlen je Schiff
        - Anzahl der umgeladenen Container pro Schiff
        - Containerzahlen je Schiff
      Welcherwert: Welche Containerzahl
      derwert: die Containerzahl
      einwert: eine Containerzahl
      Einwert: Eine Containerzahl
      allerwerte: aller Containerzahlen
      units_in:  TEU
      precision: 0
      min:       0
      max:       5000
      rundewerte: [200, 500, 1000, 1500, 2000]
      sigma:     [200, 400]
      mu:        [900, 1100]
      pop_normal:
        - die Anzahl umgeschlagener Container pro Schiff in allen Mittelmeerhäfen ungefähr normalverteilt ist
        - die Umschlagzahlen (nach Schiff) in allen Containerhäfen im Mittelmeer grob einer Normalverteilung folgen
      scene:
        - Sie untersuchen, wie viele Container umgeladen werden, wenn Feeder-Schiffe Häfen im Mittelmeer anlaufen. Container werden dabei in „twenty-foot equivalent units“ (TEU) gezählt.
        - In der Frachtschifffahrt wird das Umschlagvolumen von Containern in „twenty-foot equivalent units“ (TEU) gemessen. Sie interessieren sich für die Anzahl der umgeschlagenen Container pro Feeder-Schiff in Mittelmeerhäfen.

chiu_test:
  - scene:
      - "Die standardisierten Container, die in der Frachtschifffahrt verwendet werden, sind entweder 20 oder 40 Fuß lang. Bei einer Untersuchung im Hafen von {names[1]} wird außerdem erhoben, ob gelöschte Container ihren Zielhafen erreicht haben oder ob sie per Schiff weitertransportiert werden."
    variables:
      Containerlänge:
        values: [20 Fuß, 40 Fuß]
        weights: [3, 5]
      Zielort erreicht?:
        values: [Ja, Nein]
        weights: [3, 7]
    n: [500, 800]
    nullhypothese: "Zwischen der Containerlänge und dem Zielhafen besteht kein Zusammenhang."
    modes:
      - mode: ungerichtet
        vermuten_dass: "bei den umgeschlagenen Containern ein Zusammenhang zwischen der Länge und dem Bestimmungsort eines Containers ({names[1]} oder ein anderer Hafen) besteht."
        alternativhypothese: "Zwischen der Containerlänge und dem Zielhafen besteht ein (unbestimmter) Zusammenhang."
      - mode: gerichtet_positiv
        vermuten_dass: "bei den umgeschlagenen Containern ein Zusammenhang zwischen der Länge und dem Bestimmungsort eines Containers derart besteht, dass 40-Fuß-Container relativ häufiger weitertransportiert werden und 20-Fuß-Container eher in {names[1]} bleiben."
        alternativhypothese: "Zwischen Containerlänge (20 Fuß) und dem Zielhafen (erreicht) besteht ein positiver Zusammenhang."
      - mode: gerichtet_negativ
        vermuten_dass: "bei den umgeschlagenen Containern ein Zusammenhang zwischen der Länge und dem Bestimmungsort eines Containers derart besteht, dass 20-Fuß-Container relativ häufiger weitertransportiert werden und 40-Fuß-Container eher in {names[1]} bleiben."
        alternativhypothese: "Zwischen Containerlänge (20 Fuß) und dem Zielhafen (erreicht) besteht ein negativer Zusammenhang."

q2:
  - var: Liegezeit
  - var: Umschlag

normal:
  - var: Liegezeit
    kib_halbe: [0.5, 1, 2.5, 4]
  - var: Umschlag
    kib_halbe: [10, 25, 50, 100]

korrelation:
  - scene:
      - In der Frachtschifffahrt bezeichnet die Liegezeit die Zeit, die zwischen An- und Ablegen eines Schiffes verstreicht. Sie interessieren sich für den Zusammenhang zwischen der Liegezeit und den umgeschlagenen Containern. (Container werden in twenty-foot equivalent units oder TEU gezählt.)
      - Sie möchten in Mittelmeerhäfen untersuchen, wie sich die Anzahl der umgeschlagenen Container (gezählt in twenty-foot equivalent units oder TEU) auf die Liegezeiten von Schiffen auswirkt. Die Liegezeit ist dabei definiert als die Dauer von An- bis Ablegen eines Schiffes.
    var_x:  Umschlag
    var_x_plural: Umschlagzahlen
    var_y:  Liegezeit
    var_y_plural: Liegezeiten
    harmonize: [.5, .8]
    perfect_a: 4

zt_test:
  - var:  Liegezeit
    mode: ungerichtet
    vermuten_dass:
      - "die Abfertigungszeiten im Hafen von {names[1]} von den durchschnittlichen Zeiten abweichen."
      - "sich die Liegezeiten im Hafen von {names[1]} signifikant von der Grundgesamtheit unterscheiden."
  - var: Liegezeit
    mode: aufwärts
    vermuten_dass:
      - "Schiffe im Hafen von {names[1]} langsamer abgefertigt werden als im Gesamtdurchschnitt."
      - "die Liegezeiten im Hafen von {names[1]} signifikant länger sind als die in allen Mittelmeerhäfen."
  - var: Liegezeit
    mode: abwärts
    vermuten_dass:
      - "Feeder-Schiffe im Hafen von {names[1]} schneller abgefertigt werden als im Gesamtdurchschnitt."
      - "die Liegezeiten im Hafen von {names[1]} signifikant kürzer sind als die in allen Mittelmeerhäfen."
  - var: Umschlag
    mode: ungerichtet
    vermuten_dass:
      - "die Umschlagzahlen im Hafen von {names[1]} von den durchschnittlichen Volumina abweichen."
      - "sich die Anzahl der Container pro Schiff im Hafen von {names[1]} signifikant von der Grundgesamtheit unterscheidet."
  - var: Umschlag
    mode: aufwärts
    vermuten_dass:
      - "bei Schiffen im Hafen von {names[1]} mehr be- und entladen wird als im Gesamtdurchschnitt."
      - "die Umschlagzahlen im Hafen von {names[1]} signifikant höher sind als die in allen Mittelmeerhäfen."
  - var: Umschlag
    mode: abwärts
    vermuten_dass:
      - "Feeder-Schiffe im Hafen von {names[1]} weniger Container umschlagen als im Gesamtdurchschnitt."
      - "die Umschlagvolumina im Hafen von {names[1]} signifikant kleiner sind als die in allen Mittelmeerhäfen."

t2_test:
  - var: Liegezeit
    mode: ungerichtet
    vermuten_dass:
      - "sich die Liegezeiten in {names[1]} und {names[2]} unterscheiden."
      - "sich die Abfertigungszeiten in zwei Häfen ({names[1]} und {names[2]}) voneinander unterscheiden."
      - "die Liegezeiten in {names[1]} und {names[2]} voneinander abweichen."
  - var: Liegezeit
    mode: abwärts
    vermuten_dass:
      - "die Abfertigung in {names[1]} schneller vonstatten geht als in {names[2]}."
      - "die Liegezeiten in {names[1]} kürzer sind als in {names[2]}."
  - var: Liegezeit
    mode: aufwärts
    vermuten_dass:
      - "die Abfertigung in {names[1]} länger dauert als in {names[2]}."
      - "die durchschnittliche Liegezeit in {names[1]} größer ist als in {names[2]}."
  - var: Umschlag
    mode: ungerichtet
    vermuten_dass:
      - "sich die Umschlagzahlen pro Schiff in {names[1]} und {names[2]} unterscheiden."
      - "sich die Containerzahlen je Schiff in zwei Häfen ({names[1]} und {names[2]}) voneinander unterscheiden."
      - "die Umschlagvolumina in {names[1]} und {names[2]} voneinander abweichen."
  - var: Umschlag
    mode: abwärts
    vermuten_dass:
      - "in {names[1]} je Schiff weniger Container umgeladen werden als in {names[2]}."
      - "die Umschlagzahlen pro Schiff in {names[1]} kleiner sind als in {names[2]}."
  - var: Umschlag
    mode: aufwärts
    vermuten_dass:
      - "in {names[1]} pro Schiff mehr Container umgeladen werden als in {names[2]}."
      - "die Umschlagzahlen je Schiff in {names[1]} größer sind als in {names[2]}."

f_test:
  - var: Liegezeit
    mode: aufwärts
  - var: Liegezeit
    mode: abwärts
  - var: Liegezeit
    mode: ungerichtet
  - var: Umschlag
    mode: aufwärts
  - var: Umschlag
    mode: abwärts
  - var: Umschlag
    mode: ungerichtet
